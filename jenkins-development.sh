#!/bin/bash

set +x

/usr/bin/npm-cli-adduser --registry https://npmrepos.int.dp.xl.co.id/ --scope @xl --username xl --password Mw7WZM7FLHner3kh8tTynNcWsBNqzHEeK --email rachmasaric@xl.co.id
npm install
npm audit fix
npm run actuator
cf -v
cf api https://api.system.pcf.kb.dp.xl.co.id/ --skip-ssl-validation
cf auth developer password123
cf target -o tokoXL -s dev
APP='txl-account-service-dev'
#cf blue-green-deploy ${APP}

cf push ${APP}-new -n ${APP}
cf delete -f ${APP}
cf rename ${APP}-new ${APP}

cf delete ${APP}-old -r -f
cf set-env ${APP} OPTIMIZE_MEMORY true
cf map-route ${APP} apps.internal --hostname ${APP}
cf add-network-policy ${APP} --destination-app txl-login-controller-service-dev
cf add-network-policy txl-transaction-service-dev --destination-app ${APP}
cf add-network-policy txl-loyalty-service-dev --destination-app ${APP}