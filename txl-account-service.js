/**
* @name Account Service
* @summary Common Hydra Express service entry point
* @description Serve account services
*/
'use strict';

const { version } = require('./package.json');
const hydraExpress = require('hydra-express');
const hydra = hydraExpress.getHydra();
const express = hydraExpress.getExpress();
const stringify = require('jsesc');
const ServerResponse = require('fwsp-server-response');
const { HydraExpressLogger } = require('fwsp-logger');
hydraExpress.use(new HydraExpressLogger());
const fwspConfig = require('fwsp-config');
const helmet = require('helmet');
const session = require('cookie-session');
const apicache = require('@xl/apicache');
const redis = require('redis');
const cfenv = require('cfenv');
const actuator = require('express-cloudfoundry-actuator-middleware');
const moment = require('moment');
moment.locale('id');

function init(configFile) {
  /**
  * Load configuration file and initialize hydraExpress app
  */
  const envVars = cfenv.getAppEnv();
  let vcapApplication = {};
  let vcapService = {};
  if(envVars.isLocal) {
    vcapApplication.space_name = process.env.NODE_ENV;
  } else {
    vcapApplication = envVars.app;
    hydraExpress.appLogger.info({event: 'Looking up vcap app', message: vcapApplication.space_name});
    vcapService = envVars.services;
    hydraExpress.appLogger.info({event: 'Looking up vcap services', message: vcapService['user-provided']});
  }

  if(!configFile) {
    if (process.env.EKS_ENV === 'srg') {
      configFile = './config/config.prod.json';
    } else if (process.env.EKS_ENV === 'tgr') {
      configFile = './config/config.staging.json';
    } else if (process.env.EKS_ENV === 'jkt') {
      configFile = './config/config.dev.json';
    } else if (process.env.EKS_ENV === '') {
      configFile = './config/config.local.json';
    } else {
      configFile = './config/config.local.json';
    }
  }
 
  return fwspConfig.init(configFile).then(() => {
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
    const serverResponse = new ServerResponse();
    serverResponse.enableCORS(true);
    express.response.sendError = function(result) {
      serverResponse.sendServerError(this, {result});
    };
    express.response.sendOk = function(result) {
      serverResponse.sendOk(this, {result});
    };
    express.response.sendCreated = function(result) {
      serverResponse.sendCreated(this, {result});
    };
    express.response.sendInvalidRequest = function(result) {
      serverResponse.sendInvalidRequest(this, {result});
    };
    express.response.sendInvalidUserCredentials = function(result) {
      serverResponse.sendInvalidUserCredentials(this, {result});
    };
    express.response.sendBusinessError = function(result) {
      serverResponse.sendNotFound(this, {result});
    };
  }).then(() => {
    return hydraExpress.init(fwspConfig.getObject(), version, () => {
      hydraExpress.registerRoutes({
        '/cloudfoundryapplication': require('@xl/cf-actuator'),
        '/hystrix.stream': require('@xl/txl-hystrix-stream'),
        '/v1/account': require('./routes/account-v1-routes'),
        '/v2/account': require('./routes/account-v2-routes'),
        '/v1/inbox': require('./routes/inbox-v1-routes'),
        '/healthcheck': require('./routes/healthcheck-routes')
      });
    });
  }).then(serviceInfo => {
    const app = express.Router();
    app.use(actuator());
    app.use(helmet());
    const expiryDate = new Date(Date.now() + 60 * 60 * 1000) // 1 hour
    app.use(session({
      name: 'session',
      keys: ['key1', 'key2'],
      cookie: {
        secure: true,
        httpOnly: true,
        domain: 'xl.co.id',
        path: '/v1/account',
        expires: expiryDate
      }
    }));
    require('@xl/txl-http-logger');
    const redisCache = redis.createClient(fwspConfig.getObject().cache);
    const cacheOptions = Object.assign(fwspConfig.getObject().cache, {redisClient: redisCache});
    const cache = apicache.options(cacheOptions).middleware;
    app.use(cache(fwspConfig.getObject().cache.duration));
    hydraExpress.appLogger.info(stringify({event: 'Check EKS_ENV', message: process.env.EKS_ENV}));
    hydraExpress.appLogger.info(stringify({event: 'start up', message: serviceInfo.serviceName +' is ready using '+ configFile +' file ('+ process.env.EKS_ENV +' env) on port '+ serviceInfo.servicePort}));
  }).catch(err => hydraExpress.appLogger.error(stringify({event: 'start up', error: err.stack})));
}

module.exports = {
  init
}