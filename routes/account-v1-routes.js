/**
 * @name account-v1-api
 * @description This module packages the Content API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const hydra = hydraExpress.getHydra();
const express = hydraExpress.getExpress();
const stringify = require('jsesc');
const authenticator = require('@xl/txl-remote-authenticate');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const cfenv = require('cfenv');
const intercomm = require('@xl/cf-intercomm');
const masheryhttp = require('@xl/mashery-http');
const aes256 = require('@xl/custom-aes256');
const logger = require('@xl/txl-logger-handler');
const jClone = require('@xl/json-clone');
const cacheControl = require('@xl/cache-control');
const indirectObject = require('@xl/indirect-object-handler');
const validator = require('@xl/input-validator');
const throttle = require('@xl/txl-remote-throttle-handler');
const apicache = require('@xl/apicache');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');

const cache = apicache.middleware;

const api = express.Router();

const feCipher = new aes256();
feCipher.createCipher(config.pin.frontend);
const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

const msisdnCacheOptions = {
  appendKey: (req, res) => {
    return req.user.msisdn;
  },
  headers: {
    'cache-control': 'no-cache, no-store, must-revalidate',
    'pragma': 'no-cache',
    'expires': 0
  }
}

const msisdnCacheToggle = (req, res) => {
  return req && req.user && req.user.msisdn;
}



api.get('/balance', logger.countEvent('txl-account-service/v1/account/balance'), authenticator.remote('Bearer'), cacheControl.disable, (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request user balance'}));
  const { user } = req;
  const fallbackOptions = {};
  const lang = req.headers.language;
  const requestOptions = jClone.clone(config.apis['robalance']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['msisdn', user.msisdn]);
  hystrixutil.request('robalance', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
    if(result && result.data && result.data.data && result.data.data.balances) {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get user balance', message: 'Get user balance success'}));
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: result.data.data.balances});
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get user balance', message: 'Get user balance failed', data: result.data}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    }
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get user balance', message: 'Error getting user balance', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
    res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
  });
});

api.get('/profile', logger.countEvent('txl-account-service/v1/account/profile'), authenticator.remote('Bearer'), cache('1 week', msisdnCacheToggle, msisdnCacheOptions), (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request user profile'}));
  const { user } = req;
  const fallbackOptions = {};
  const authCode = req.headers.authorization;
  const lang = req.headers.language;
  const requestOptions = jClone.clone(config.apis['roprofile']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['msisdn', user.msisdn]);
  hystrixutil.request('roprofile', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
    if(result && result.data && result.data.data && result.data.data.profile) {
      result.data.data.profile.account_cd = indirectObject.encrypt(beCipher, result.data.data.profile.account_cd, authCode);
      result.data.data.profile.dealer_id = indirectObject.encrypt(beCipher, result.data.data.profile.dealer_id, authCode);
      result.data.data.profile.parent.parent_account_cd = indirectObject.encrypt(beCipher, result.data.data.profile.parent.parent_account_cd, authCode);
      result.data.data.profile.account_name = result.data.data.profile.account_name ? result.data.data.profile.account_name : '';
      result.data.data.profile.owner_name = result.data.data.profile.owner_name ? result.data.data.profile.owner_name : '';
      result.data.data.profile.tax_registration_no = result.data.data.profile.tax_registration_no ? result.data.data.profile.tax_registration_no : '';
      result.data.data.profile.email = result.data.data.profile.email ? result.data.data.profile.email : '';
      result.data.data.profile.address = result.data.data.profile.address ? result.data.data.profile.address : '';
      result.data.data.profile.owner_id_num = result.data.data.profile.owner_id_num ? result.data.data.profile.owner_id_num.replace(/[^0-9]/g, "") : '';
      delete result.data.data.profile.longitude;
      delete result.data.data.profile.latitude;
      // show address for get data ktp
      // delete result.data.data.profile.address;
      delete result.data.data.profile.contact_no;
      delete result.data.data.profile.parent.parent_msisdn;
      apicache.clear('/v1/account/profile$$appendKey='+ user.msisdn);
      apicache.clear('/v1/auth/profile/'+ user.msisdn);
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get user profile', message: 'Get user profile success'}));
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm, data: result.data.data.profile});
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Get user profile', message: 'Get user profile failed', data: result.data}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    }
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Get user profile', message: 'Error getting user profile', error: '['+error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
    res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
  });
});

api.put('/pin', logger.countEvent('txl-account-service/v1/account/change-pin'), authenticator.disable(rc, '51'), authenticator.remote('Bearer'), validator.validate([['req.body.oldPin', validator.NOT_NULL_TYPE], ['req.body.newPin', validator.NOT_NULL_TYPE]]), cacheControl.disable, (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request change pin'}));
  const { user } = req;
  try {
    let oldPin = feCipher.decrypt(req.body.oldPin);
    let newPin = feCipher.decrypt(req.body.newPin);
    oldPin = beCipher.encrypt(oldPin);
    newPin = beCipher.encrypt(newPin);
    const fallbackOptions = {};
    const lang = req.headers.language;
    const requestOptions = jClone.clone(config.apis['rochangepin']);
    requestOptions.headers.event_id = req.logId;
    requestOptions.headers.actor = user.msisdn;
    const querys = [];
    querys.push(['msisdn', user.msisdn]);
    querys.push(['oldPin', oldPin]);
    querys.push(['newPin', newPin]);
    hystrixutil.request('rochangepin', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
      if(result && result.data && result.data.data) {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Change pin', message: 'Change pin success'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Change pin', message: 'Change pin failed', data: result.data}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
        res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
      }
    }).catch(error => {
      if(error.message.status === 401) {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Change pin', message: 'PIN not match', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
        res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
      } else {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Change pin', message: 'Error changing pin', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    });
  } catch(error) {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Change pin', message: 'PIN not match', error: error.message, stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
    res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
  }
  
});

api.get('/pin/:pin', logger.countEvent('txl-account-service/v1/account/validate-pin'), authenticator.remote('Bearer'), cacheControl.disable, (req, res) => {
  hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request validate pin'}));
  const lang = req.headers.language;
  const { user } = req;
  const msisdn = user ? user.msisdn : null;
  const type = 'validpin';
  intercommThrottle(req, type).then(response => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Calling intercomm throttle service', message: 'Throttle service success'}));
    validatePIN(req, res, type);
  }).catch(error => {
    hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Calling intercomm throttle service', message: 'Error calling intercomm throttle service', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Calling intercomm unthrottle service', message: 'Destroy throttling'}));
    intercommUnthrottle(req, type).then(result => {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Calling intercomm logout service', message: 'Force logout'}));
      intercommLogout(req);
    });
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Force logout'}));
    res.sendInvalidUserCredentials({errorCode: rc.codes['14'].rc, errorMessage: rc.i18n('14', lang).rm});
  });
});

function validatePIN(req, res, type) {
  const fallbackOptions = {};
  const { user } = req;
  const lang = req.headers.language;
  let { pin } = req.params;
  pin = indirectObject.decrypt(feCipher, pin, null);
  pin = beCipher.encrypt(pin);
  const requestOptions = jClone.clone(config.apis['rovalidatepin']);
  requestOptions.headers.event_id = req.logId;
  requestOptions.headers.actor = user.msisdn;
  const querys = [];
  querys.push(['msisdn', user.msisdn]);
  querys.push(['pin', pin]);
  hystrixutil.request('rovalidatepin', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
    if(result && result.data && result.data.data) {
      intercommUnthrottle(req, type);
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate pin', message: 'Validate pin success'}));
      res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
    } else {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate pin', message: 'PIN not match'}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
      res.sendOk({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
    }
  }).catch(error => {
    if(error.message.status === 401) {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate pin', message: 'PIN not match', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'PIN not match'}));
      res.sendOk({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
    } else {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate pin', message: 'Error validating pin', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    }
  });
}

function intercommThrottle(req, type) {
  const { user } = req;
  const throttleId = user.msisdn;
  const maxRetry = 5;
  const duration = 30;
	const authHeader = req.headers.authorization;
  const envVars = cfenv.getAppEnv();
  
  const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-'+ envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV +'-' : '');
	const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service'+ environment : environment +'txl-login-controller-service';
	const apiNameService = 'throttle';
	const apiQuerys = [['authorizationHeader', authHeader], ['throttleId', throttleId], ['type', type], ['maxretry', maxRetry], ['duration', duration]];
  
  const apiOptions = {
    throttle: {
      'url': '/v1/login/token/throttle/${throttleId}?type=${type}&duration=${duration}&maxretry=${maxretry}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    unthrottle: {
      url: '/v1/login/token/unthrottle/${throttleId}?type=${type}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    logout: {
      url: '/v1/auth/logout',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    }
  };

  if(!envVars.isLocal) {
		hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'PCF '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://txl-login-controller-service'+ environment +'.apps.internal:8080'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Throttling', message: 'Intercomm increment throttle'}));
    return intercomm.request(options);
  } else if(process.env.EKS_ENV) {
		hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'EKS '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://'+ environment +'txl-login-controller-service:8080'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Throttling', message: 'Intercomm increment throttle'}));
    return intercomm.request(options);
  } else {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'EKS '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://localhost:8082'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Throttling', message: 'Intercomm increment throttle'}));
    return intercomm.request(options);
  }
}

function intercommUnthrottle(req, type) {
  const { user } = req;
  const throttleId = user.msisdn;
	const authHeader = req.headers.authorization;
  const envVars = cfenv.getAppEnv();
  
  const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-'+ envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV +'-' : '');
	const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service'+ environment : environment +'txl-login-controller-service';
	const apiNameService = 'unthrottle';
	const apiQuerys = [['authorizationHeader', authHeader], ['throttleId', throttleId], ['type', type]];
  
  const apiOptions = {
    throttle: {
      'url': '/v1/login/token/throttle/${throttleId}?type=${type}&duration=${duration}&maxretry=${maxretry}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    unthrottle: {
      url: '/v1/login/token/unthrottle/${throttleId}?type=${type}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    logout: {
      url: '/v1/auth/logout',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    }
  };

  if(!envVars.isLocal) {
		hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'PCF '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://txl-login-controller-service'+ environment +'.apps.internal:8080'
        },
        apis: apiOptions
      }		
    };
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Throttling', message: 'Intercomm destroy throttle'}));
    return intercomm.request(options);
  } else if(process.env.EKS_ENV) {
		hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'EKS '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://'+ environment +'txl-login-controller-service:8080'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Throttling', message: 'Intercomm destroy throttle'}));
    return intercomm.request(options);
  } else {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'EKS '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://localhost:8082'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Throttling', message: 'Intercomm destroy throttle'}));
    return intercomm.request(options);
  }
}

function intercommLogout(req) {
  const { user } = req;
	const authHeader = req.headers.authorization;
	const envVars = cfenv.getAppEnv();

  const environment = !envVars.isLocal ? (envVars.app.space_name == 'prod' ? '' : '-'+ envVars.app.space_name) : (process.env.EKS_ENV && process.env.EKS_ENV !== '' ? process.env.EKS_ENV +'-' : '');
	const apiServiceName = !envVars.isLocal ? 'txl-login-controller-service'+ environment : environment +'txl-login-controller-service';
	const apiNameService = 'logout';
	const apiQuerys = [['authorizationHeader', authHeader]];
  
  const apiOptions = {
    throttle: {
      'url': '/v1/login/token/throttle/${throttleId}?type=${type}&duration=${duration}&maxretry=${maxretry}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    unthrottle: {
      url: '/v1/login/token/unthrottle/${throttleId}?type=${type}',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    },
    logout: {
      url: '/v1/auth/logout',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': '${authorizationHeader}'
      },
      circuitBreakerSleepWindowInMilliseconds: 5000,
      circuitBreakerRequestVolumeThreshold: 5,
      circuitBreakerForceOpened: false,
      circuitBreakerForceClosed: false,
      circuitBreakerErrorThresholdPercentage: 60,
      statisticalWindowLength: 10000,
      statisticalWindowNumberOfBuckets: 10,
      requestVolumeRejectionThreshold: 100,
      timeout: 5000
    }
  };

  if(!envVars.isLocal) {
		hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'PCF '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://txl-login-controller-service'+ environment +'.apps.internal:8080'
        },
        apis: apiOptions
      }		
    };
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Logout', message: 'Intercomm logout'}));
    return intercomm.request(options);
  } else if(process.env.EKS_ENV) {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'EKS '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://'+ environment +'txl-login-controller-service:8080'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Logout', message: 'Intercomm logout'}));
    return intercomm.request(options);
  } else {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Check environment', message: 'EKS '+ environment +' environment'}));
		const options = {
			serviceName: apiServiceName, 
			apiName: apiNameService, 
			querys: apiQuerys,
			serviceOptions : {
        credentials : {
          url: 'http://localhost:8082'
        },
        apis: apiOptions
      }
		};
		if(options.serviceOptions && options.serviceOptions.apis && options.serviceOptions.apis[apiNameService]) {
			options.serviceOptions.apis[apiNameService].headers.event_id = req.logId;
			options.serviceOptions.apis[apiNameService].headers.actor = user.msisdn;
    }
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Logout', message: 'Intercomm logout'}));
    return intercomm.request(options);
  }
}

function fallback(error, args) {
  return Promise.reject({
		message: error.message,
		args
	});
}


module.exports = api;