/**
 * @name account-v2-api
 * @description This module packages the Content API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const express = hydraExpress.getExpress();
const authenticator = require('@xl/txl-remote-authenticate');
const logger = require('@xl/txl-logger-handler');
const cacheControl = require('@xl/cache-control');
const validator = require('@xl/input-validator');
const throttle = require('@xl/txl-remote-throttle-handler');
const changePin = require('../services/change-pin-v2-service');
const resetPin = require('../services/reset-pin-v2-service');
const ktpService = require('../services/ktp-v2-service');
const apicache = require('@xl/apicache');
const cache = apicache.middleware;
const rc = require('../constant/RC');

const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({ filename(req, file, cb) { cb(null, file.originalname); } });
const upload = multer({ dest: path.join(__dirname, '../tmp'), storage, limits: { fileSize: 2000000 } });
const fileSizeLimitErrorHandler = (err, req, res, next) => {
  const lang = req.headers.language;
  if (err) {
    res.sendBusinessError({ errorCode: rc.codes['10'].rc, errorMessage: rc.i18n('10', lang).rm });
  } else {
    next()
  }
};
const roMini = require('../services/ro-mini-v2-service');

const api = express.Router();



api.post('/pin', logger.countEvent('txl-account-service/v1/account/generate-otp-change-pin'), authenticator.remote('Bearer'), cacheControl.disable, changePin.generateOtpV2);

api.put('/pin', logger.countEvent('txl-account-service/v1/account/change-pin'), authenticator.remote('Bearer'), validator.validate([['req.body.otp', validator.ALPHANUMERIC_TYPE], ['req.body.oldPin', validator.NOT_NULL_TYPE], ['req.body.newPin', validator.NOT_NULL_TYPE]]), cacheControl.disable, changePin.changePin);

api.post('/reset-pin', logger.countEvent('txl-account-service/v1/account/generate-otp-reset-pin'), authenticator.remote('Bearer'), cacheControl.disable, resetPin.generateOtpV2);

api.put('/reset-pin', logger.countEvent('txl-account-service/v1/account/reset-pin'), authenticator.remote('Bearer'), validator.validate([['req.body.otp', validator.ALPHANUMERIC_TYPE]]), throttle.countMiddleware(resetPin.throttleToggle, resetPin.throttleKeyMaker, 'resetpinconstraint', 1, 5, resetPin.throttleErrorResponse), cacheControl.disable, resetPin.resetPin);

api.get('/profile/ktp', logger.countEvent('txl-account-service/v2/account/profile/ktp'), authenticator.remote('Bearer'), cacheControl.disable, cache(ktpService.getCacheDurationInMillis, ktpService.getCacheToggle, ktpService.cacheOptions), ktpService.getProfileKtp);

api.post('/profile/update-ktp', logger.countEvent('txl-account-service/v2/account/profile/update-ktp'), authenticator.remote('Bearer'), upload.array('files', 2), fileSizeLimitErrorHandler, ktpService.updateKtp);

api.get('/ro-mini/profile', logger.countEvent('txl-account-service/v2/account/ro-mini/profile'), authenticator.remote('Bearer'), roMini.getRoMiniUserByEmail);

api.post('/ro-mini/send-otp', logger.countEvent('txl-account-service/v2/account/ro-mini/send-otp'), authenticator.remote('Bearer'), cacheControl.disable, roMini.sendOtpEmail);

api.put('/ro-mini/reset-pin', logger.countEvent('txl-account-service/v2/account/ro-mini/reset-pin'), authenticator.remote('Bearer'), validator.validate([['req.body.email', validator.EMAIL_TYPE], ['req.body.otp', validator.NUMERIC_TYPE]]), throttle.countMiddleware(resetPin.throttleToggle, resetPin.throttleKeyMaker, 'resetpinconstraint', 1, 5, resetPin.throttleErrorResponse), cacheControl.disable, roMini.resetPin);

api.put('/ro-mini/change-pin', logger.countEvent('txl-account-service/v2/account/ro-mini/change-pin'), authenticator.remote('Bearer'), validator.validate([['req.body.email', validator.EMAIL_TYPE], ['req.body.otp', validator.NUMERIC_TYPE]]), cacheControl.disable, roMini.changePin);

api.post('/ro-mini/pin', logger.countEvent('txl-account-service/v2/account/ro-mini/validate-pin'), authenticator.remote('Bearer'), cacheControl.disable, roMini.validatePin);

api.get('/profile/ro-mini/ktp', logger.countEvent('txl-account-service/v2/account/profile/ktp'), authenticator.remote('Bearer'), cacheControl.disable, cache(ktpService.getCacheDurationInMillis, ktpService.getCacheToggle, ktpService.cacheOptions), ktpService.getProfileKtpRoMini);

api.post('/profile/ro-mini/update-ktp', logger.countEvent('txl-account-service/v2/account/profile/ro-mini/update-ktp'), authenticator.remote('Bearer'), upload.array('files', 2), fileSizeLimitErrorHandler, ktpService.updateKtpRoMini);


module.exports = api;