/**
 * @name inbox-v1-api
 * @description This module packages the Content API.
 */
'use strict';

const hydraExpress = require('hydra-express');
const express = hydraExpress.getExpress();
const authenticator = require('@xl/txl-remote-authenticate');
const config = require('fwsp-config').getObject();
const Aes256 = require('@xl/custom-aes256');
const logger = require('@xl/txl-logger-handler');
const cacheControl = require('@xl/cache-control');
const indirectObject = require('@xl/indirect-object-handler');
const validator = require('@xl/input-validator');
const apicache = require('@xl/apicache');
const inboxService = require('../services/inbox-v2-service');

const cache = apicache.middleware;

const api = express.Router();

const beCipher = new Aes256();
beCipher.createCipher(config.pin.backend);



api.get('/', logger.countEvent('txl-account-service/v1/inbox/list'), authenticator.remote('Bearer'), cache(inboxService.getCacheDurationInMillis, inboxService.getCacheToggle, inboxService.cacheOptions), inboxService.list);

api.get('/:mailboxid', logger.countEvent('txl-account-service/v1/inbox/detail'), authenticator.remote('Bearer'), indirectObject.decryptParameter(beCipher, ['req.params.mailboxid'], false), validator.validate([['req.params.mailboxid', validator.NUMERIC_TYPE]]), cache(inboxService.getCacheDurationInMillis, null, inboxService.cacheOptions), inboxService.detail);

api.delete('/:mailboxid', logger.countEvent('txl-account-service/v1/inbox/delete'), authenticator.remote('Bearer'), indirectObject.decryptParameter(beCipher, ['req.params.mailboxid'], false), validator.validate([['req.params.mailboxid', validator.NUMERIC_TYPE]]), cacheControl.disable, inboxService.delete);


module.exports = api;