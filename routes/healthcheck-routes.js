/**
 * @name account-healthcheck
 * @description This module packages account healthcheck.
 */
'use strict';

const hydraExpress = require('hydra-express');
const dateFormatter = require('@xl/util-date');
const express = hydraExpress.getExpress();
const api = express.Router();

api.get('/', async (req, res) => {
    const healthcheck = {
        uptime: await toHHMMSS(process.uptime()),
        message: 'OK',
        timestamp: dateFormatter.formatCommonDateTime(new Date())
    };
    try {
        res.send(healthcheck);
    } catch (e) {
        healthcheck.message = e;
        res.status(503).send();
    }
});

const toHHMMSS = (uptime) => {
    const sec = parseInt(uptime, 10)
    const hours = Math.floor(sec / 3600)
    const minutes = Math.floor(sec / 60) % 60
    const seconds = sec % 60

    return [hours, minutes, seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v, i) => v !== "00" || i > 0)
        .join(":")
}


module.exports = api;