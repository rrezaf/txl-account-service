const hydraExpress = require('hydra-express');
const stringify = require('jsesc');
const hystrixutil = require('@xl/hystrix-util');
const config = require('fwsp-config').getObject();
const uuidv1 = require('uuid/v1');
const masheryhttp = require('@xl/mashery-http');
const aes256 = require('@xl/custom-aes256');
const jClone = require('@xl/json-clone');
const dateutil = require('@xl/util-date');
const rc = require('../constant/RC');
const apigatehttp = require('@xl/apigate-http');

const feCipher = new aes256();
feCipher.createCipher(config.pin.frontend);
const beCipher = new aes256();
beCipher.createCipher(config.pin.backend);

module.exports = {
  generateOtp: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request generate OTP for reset pin'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const requestId = uuidv1();
    const requestDate = dateutil.formatCommonDateTime(new Date());
    const otpRequestOptions = jClone.clone(config.apis['generateotp']);
    otpRequestOptions.headers.event_id = req.logId;
    otpRequestOptions.headers.actor = user.msisdn;
    const otpQuerys = [];
    otpQuerys.push(['requestId', requestId]);
    otpQuerys.push(['requestDate', requestDate]);
    otpQuerys.push(['msisdn', user.msisdn]);
    otpQuerys.push(['channel', 'TOKOXLAPPRESETPIN']);
    hystrixutil.request('generateotp', otpRequestOptions, masheryhttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
      const responseCode = otpResult && otpResult.data && otpResult.data.opSendOtpRs ? otpResult.data.opSendOtpRs.headerRs.responseCode : '01';
      if(responseCode === '00') {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for reset pin', message: 'Generate OTP for reset pin success'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for reset pin sent'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for reset pin', message: 'Generate OTP for reset pin failed', data: otpResult.data}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for reset pin failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Generate OTP for reset pin', message: 'Generate OTP for reset pin failed', error: '['+ error.message.status +'] '+ JSON.stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reaching API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  generateOtpV2: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request generate OTP for reset pin'}));
    const { user } = req;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const lang = req.headers.language;
    const otpRequestOptions = jClone.clone(config.apis['generateotpV2']);
    otpRequestOptions.headers.event_id = req.logId;
    otpRequestOptions.headers.actor = user.msisdn;
    const otpQuerys = [];
    otpQuerys.push(['msisdn', user.msisdn]);
    otpQuerys.push(['channel', 'TOKOXLAPPRESETPIN']);
    hystrixutil.request('generateotpV2', otpRequestOptions, apigatehttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
      const responseCode = otpResult && otpResult.data && otpResult.data.reason;
      if(responseCode === 'Success') {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for reset pin', message: 'Generate OTP for reset pin success'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for reset pin sent'}));
        res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Generate OTP for reset pin', message: 'Generate OTP for reset pin failed', data: otpResult.data}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'OTP for reset pin failed'}));
        res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
      }
    }).catch(error => {
      hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Generate OTP for reset pin', message: 'Generate OTP for reset pin failed', error: '['+ error.message.status +'] '+ JSON.stringify(error.message.data), stack: error.stack}));
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reaching API failed'}));
      res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
    });
  },
  resetPin: (req, res) => {
    hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Request', message: 'Request reset pin'}));
    const lang = req.headers.language;
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const { user } = req;
    module.exports.validateOTPV2(req, 'TOKOXLAPPRESETPIN').then(result => {
      if(result) {
        const requestOptions = jClone.clone(config.apis['roresetpin']);
        requestOptions.headers.event_id = req.logId;
        requestOptions.headers.actor = user.msisdn;
        const querys = [];
        querys.push(['msisdn', user.msisdn]);
        hystrixutil.request('roresetpin', requestOptions, apigatehttp.request, fallbackOptions, fallback, querys).then(result => {
          if(result && result.data && result.data.data) {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Reset pin', message: 'Reset pin success'}));
            res.sendOk({errorCode: rc.codes['00'].rc, errorMessage: rc.i18n('00', lang).rm});
          } else {
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Reset pin', message: 'Reset pin failed', data: result.data}));
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reset pin failed'}));
            res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
          }
        }).catch(error => {
          if(error.message.status === 401) {
            hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Reset pin', message: 'Reset pin failed', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reset pin failed'}));
            res.sendBusinessError({errorCode: rc.codes['11'].rc, errorMessage: rc.i18n('11', lang).rm});
          } else {
            hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Reset pin', message: 'Error reset pin', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
            hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Reach API failed'}));
            res.sendError({errorCode: rc.codes['91'].rc, errorMessage: rc.i18n('91', lang).rm});
          }
        });
      } else {
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed'}));
        hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Response', message: 'Validate SMS OTP failed'}));
        res.sendBusinessError({errorCode: rc.codes['15'].rc, errorMessage: rc.i18n('15', lang).rm});
      }
    });
  },
  validateOTP: (req, channel) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const { user } = req;
    const { otp } = req.body;
    let bypassStatus = false;
    if(config.otpWhitelist) {
      for(let i = 0; i < config.otpWhitelist.length; i++) {
        if(config.otpWhitelist[i].msisdn === user.msisdn && config.otpWhitelist[i].otp === otp) {
          bypassStatus = true;
          break;
        }
      }
    }
    if(bypassStatus) {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
      return true;
    } else {
      const requestOptions = jClone.clone(config.apis['validateotp']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const otpQuerys = [];
      otpQuerys.push(['msisdn', user.msisdn]);
      otpQuerys.push(['otp', otp]);
      otpQuerys.push(['channel', channel]);
      return hystrixutil.request('validateotp', requestOptions, masheryhttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
        if(otpResult && otpResult.data && otpResult.data.headerRs && otpResult.data.headerRs.responseCode && otpResult.data.headerRs.responseCode === '00') {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
          return true;
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed'}));
          return false;
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Error validating SMS OTP', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        return false;
      });
    }
  },
  validateOTPV2: (req, channel) => {
    const fallbackOptions = {};
    const fallback = (error, args) => {
      return Promise.reject({
        message: error.message,
        args
      });
    };
    const { user } = req;
    const { otp } = req.body;
    let bypassStatus = false;
    if(config.otpWhitelist) {
      for(let i = 0; i < config.otpWhitelist.length; i++) {
        if(config.otpWhitelist[i].msisdn === user.msisdn && config.otpWhitelist[i].otp === otp) {
          bypassStatus = true;
          break;
        }
      }
    }
    if(bypassStatus) {
      hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
      return true;
    } else {
      const requestOptions = jClone.clone(config.apis['validateotpV2']);
      requestOptions.headers.event_id = req.logId;
      requestOptions.headers.actor = user.msisdn;
      const otpQuerys = [];
      otpQuerys.push(['msisdn', user.msisdn]);
      otpQuerys.push(['otp', otp]);
      otpQuerys.push(['channel', channel]);
      return hystrixutil.request('validateotpV2', requestOptions, apigatehttp.request, fallbackOptions, fallback, otpQuerys).then(otpResult => {
        const responseCode = otpResult && otpResult.data && otpResult.data.reason;
        if(responseCode === 'OK') {          
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP success'}));
          return true;
        } else {
          hydraExpress.appLogger.info(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Validate SMS OTP failed'}));
          return false;
        }
      }).catch(error => {
        hydraExpress.appLogger.error(stringify({id: req.logId, event: 'Validate SMS OTP', message: 'Error validating SMS OTP', error: '['+ error.message.status +'] '+ stringify(error.message.data), stack: error.stack}));
        return false;
      });
    }
  },
  throttleErrorResponse: {
    status: 404, 
    data: {
      errorCode: '19', 
      errorMessage: {
        en: 'Reset PIN allowed after 5 minutes.', 
        id: 'Reset PIN hanya diperbolehkan setelah 5 menit.',
        in: 'Reset PIN hanya diperbolehkan setelah 5 menit.'
      }
    }
  }, 
  throttleToggle: (req, res) => {
    const msisdnA = req.user && req.user.msisdn;
    return msisdnA;
  }, 
  throttleKeyMaker: (req, res) => {
    const msisdnA = req.user && req.user.msisdn;
    return msisdnA;
  }
}