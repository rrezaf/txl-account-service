import http from 'k6/http';
//import uuid from './uuid.js';

export default function () {
    var url = 'https://tgr-txl-account-service.ext.dp.xl.co.id/v1/account/profile'

    var payload = JSON.stringify({
    token: '1234567890'
    });

    var params = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer abcd'
        },
        timeout: 10000
    };

    var startTime = new Date();

    //http.post(url, payload, params);
    http.get(url, params);
}
